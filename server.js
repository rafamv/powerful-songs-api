const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.json({
        "message": "This is the greatest API for music search"
    });
});

app.listen(3000, () => {
    console.log("API is up and running on port 3000")
});